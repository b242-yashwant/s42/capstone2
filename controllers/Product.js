const Product = require("../models/Product");

module.exports.createProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product ({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else {
				return true;
			};
		});
	}
	else {
		return false;
	};
};

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


module.exports.getAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}
module.exports.updateProduct = (reqParams,data) => {
	if(data.isAdmin){
	let updatedProduct = {
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course,error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
	}
	else {
		return false;
	}
}

module.exports.archiveProduct = (reqParams, reqBody) => {
	
		let updateActiveField = {
		isActive : reqBody.isActive
	};


	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product,error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	});
}

module.exports.activateProduct = (reqParams, data) => {
	if(data.isAdmin){
	let activatedProduct = {
		isActive : true
	};
	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((course,error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
	}
	else {
		return false;
	}
}