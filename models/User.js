// User
//  - Email (String)
//  - Password (String)
//  - isAdmin (Boolean - defaults to false)
//  - orders (Array of Objects)
//    - products (Array of Objects)
//      - productName (String)
//      - quantity (Number)
//    - totalAmount (Number)
//    - purchasedOn (Date - defaults to current timestamp)

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email : {
    type : String,
    required : [true, "Email is required"]
  },
  password : {
    type : String,
    required : [true, "Password is required"]
  },
  isAdmin : {
    type : Boolean,
    default : false
  },
  mobileNo : {
    type : String,
  }
})

module.exports = mongoose.model("User", userSchema);