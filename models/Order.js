const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type: String,
		// required : [true, "userId is required"]
	},
	products : [
  		{
                productId : {
                    type : String
                  },
                quantity :  {
                type : Number

                  }
        }
	  ],
	  totalAmount: {
	        type : Number,
	        // required : [true, " Total Amount is required"]
	  },
	  puchasedOn: {
	        type : Date,
	        default : new Date()
	  }
})


module.exports = mongoose.model("Order", orderSchema);