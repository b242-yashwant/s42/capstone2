const express = require("express");
const router = express.Router();
const Product = require("../models/Product");
const productControllers = require("../controllers/Product");
const auth = require("../auth");


router.post("/", auth.verify, (req,res) => {
	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}	
	productControllers.createProduct(data).then(resultFromController => res.send(resultFromController));

})

router.get("/allProducts", (req,res) => {
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})


router.get("/", (req,res) => {
	productControllers.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req,res) => {
	productControllers.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId", (req,res) => {
	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId/archive", (req,res) => {
	// const data = {
	// 	isAdmin : auth.decode(req.headers.authorization).isAdmin
	// }
	productControllers.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId/activate", (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.activateProduct(req.params, data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;


