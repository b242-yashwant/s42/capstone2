const express = require("express");
const router = express.Router();
const userController = require("../controllers/User");
const auth = require("../auth");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/userAuth",(req,res) => {
	userController.userAuth(req.body).then(resultFromController => res.send(resultFromController));
})

// router.post("/checkout",(req,res) => {

// })

router.get("/userDetails", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getUserDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

router.post("/checkout", auth.verify, (req,res) => {
	userController.createOrder(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/orders", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
})

router.get("/myOrders", auth.verify, (req,res) => {
	let data  = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.getMyOrders(data).then(resultFromController => res.send(resultFromController));

})
router.put("/:userId/setAsAdmin",auth.verify, (req,res) => {
	let data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;