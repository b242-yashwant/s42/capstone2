const express=require("express");
const mongoose= require("mongoose");
const cors=require("cors");

const UserRoutes= require("./routes/User");
const productRoutes=require("./routes/Product");


const app=express();

mongoose.connect("mongodb+srv://yashwant:LE13OL0dnLVV0YOy@zuitt-bootcamp.bbfflap.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
});

mongoose.connection.once('open',()=>console.log('now connected to mongodb atlas'));

app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/Users", UserRoutes);
app.use("/products", productRoutes);



app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});
